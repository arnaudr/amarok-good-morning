[Desktop Entry]
Icon=weather-clear
Type=script
ServiceTypes=KPluginInfo

Name=Good Morning
Comment=Wake up with music

X-KDE-PluginInfo-Name=Good Morning
X-KDE-PluginInfo-Version=0.1
X-KDE-PluginInfo-Category=Generic
X-KDE-PluginInfo-Author=El Boulangero
X-KDE-PluginInfo-Email=elboulangero@gmail.com
X-KDE-PluginInfo-Depends=Amarok2.6
X-KDE-PluginInfo-License=GPLV3
X-KDE-PluginInfo-EnabledByDefault=false
