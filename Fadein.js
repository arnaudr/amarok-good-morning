/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-morning amarok script.                       */
/*                                                                            */
/* good-morning is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-morning is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-morning.  If not, see <http://www.gnu.org/licenses/>.      */
/******************************************************************************/

Importer.include("VolumeMonitor.js");

function Fadein()
{
    Amarok.debug("-> " + arguments.callee.name);

// Private
    // Variables for changing volume
    this.step = 0;
    this.current = 0;
    this.endVolume = 0;

    // Fade-in timer - called every second to adjust volume
    this.timer = new QTimer();
    this.timer.timeout.connect(this, this.timeout);

    // Volume monitor
    // Allows to stop fade-in if user manually changes the volume.
    // We need to distinguish between volume changed by user, and volume changed
    // by us during fade-in.
    // We do that by "muting" the Amarok.Engine.volumeChanged signal, so that
    // after we change the volume, we ignore the next volumeChanged signal.
    // The problem is that, sometimes, we get notified twice by
    // Amarok.Engine.volumeChanged.
    // So, we must mute the signal for the two next incoming signals, knowing that
    // we may miss a change made by user.
    this.volumeMonitor = new VolumeMonitor();

    Amarok.debug("<- " + arguments.callee.name);
}

Fadein.prototype.start = function(seconds, volume)
{
    Amarok.debug("-> Fadein." + "start at " + new Date());

    // Initialize fade-in variables
    this.endVolume = volume;
    this.step = volume / seconds;
    this.current = 0;

    // Set volume
    Amarok.Engine.volume = 0;

    // Initialize volume monitor
    this.volumeMonitor.connect(this, this.stop);

    // Start timer
    this.timer.start(1000);

    Amarok.debug("<- Fadein." + "start");
}

Fadein.prototype.stop = function()
{
    Amarok.debug("-> Fadein." + "stop");

    this.timer.stop();
    this.volumeMonitor.disconnect();

    Amarok.debug("<- Fadein." + "stop");
}

// Private methods

Fadein.prototype.timeout = function()
{
//    Amarok.debug("-> Fadein." + "timeout");

    // Increment value to add
    this.current += this.step;

    // Increase volume only if value is > 1, since
    // Amarok.Engine.IncreaseVolume paremeter is an integer
    if (this.current >= 1) {
	// Inhibit the volume monitor for 2 signals
	this.volumeMonitor.muteFor(2);

	// Change volume
	var round = Math.floor(this.current);
	Amarok.Engine.IncreaseVolume(round);
	this.current -= round;
    }

    // Check if fade-in is finished
    if (Amarok.Engine.volume >= this.endVolume) {
	Amarok.debug("Fade-in stopped at " + new Date());
	this.stop();
    }

//    Amarok.debug("<- Fadein." + "timeout");
}
