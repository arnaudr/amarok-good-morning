#!/bin/sh

BASENAME=$(basename $0)
SECONDS=$1

if [ $# -ne 1 ]; then
    echo "Usage: $0 <seconds>"
    exit 1
fi

RTCWAKE=$(whereis -b rtcwake | cut -d' ' -f2)
if [ -z "$RTCWAKE" ]; then
    echo >&2 "rtcwake is not installed."
    exit 1
fi

SUDO=""
if [ "$BASENAME" = "rtcwake-setuid.sh" ]; then
    if [ ! -u "$RTCWAKE" ]; then
	echo >&2 "rtcwake is not setuid."
	exit 1
    fi
elif [ "$BASENAME" = "rtcwake-sudo.sh" ]; then
    if ! command -v sudo >/dev/null 2>&1; then
	echo >&2 "sudo is not installed."
	exit 1
    fi
    SUDO=sudo
else
    echo >&2 "This script must be called from a symlink."
    exit 1
fi

if [ $SECONDS -gt 0 ]; then
    $SUDO rtcwake --mode no --seconds $SECONDS
else
    $SUDO rtcwake --mode disable
fi