/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-morning amarok script.                       */
/*                                                                            */
/* good-morning is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-morning is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-morning.  If not, see <http://www.gnu.org/licenses/>.      */
/******************************************************************************/

function Gui()
{
    Amarok.debug("-> " + arguments.callee.name);

    // Load ui file
    var uiLoader = new QUiLoader(this);
    var uiFile = new QFile(Amarok.Info.scriptPath() + "/Gui.ui");
    uiFile.open(QIODevice.ReadOnly);
    this.dialog = uiLoader.load(uiFile, this);

    // Connect browse button
    this.dialog.rtcGroupBox.browseButton.clicked.connect(this, this.browse);

    Amarok.debug("<- " + arguments.callee.name);
}

Gui.prototype.setValues = function(config)
{
    Amarok.debug("-> Gui." + "setValues");

    var gui, conf;

    // Usually
    gui = this.dialog.timeGroupBox.usualGroupBox;
    conf = config.usual;

    gui.mondayCheckBox.setChecked(conf[0].enabled);
    gui.mondayTimeEdit.setTime(conf[0].time);
    gui.tuesdayCheckBox.setChecked(conf[1].enabled);
    gui.tuesdayTimeEdit.setTime(conf[1].time);
    gui.wednesdayCheckBox.setChecked(conf[2].enabled);
    gui.wednesdayTimeEdit.setTime(conf[2].time);
    gui.thursdayCheckBox.setChecked(conf[3].enabled);
    gui.thursdayTimeEdit.setTime(conf[3].time);
    gui.fridayCheckBox.setChecked(conf[4].enabled);
    gui.fridayTimeEdit.setTime(conf[4].time);
    gui.saturdayCheckBox.setChecked(conf[5].enabled);
    gui.saturdayTimeEdit.setTime(conf[5].time);
    gui.sundayCheckBox.setChecked(conf[6].enabled);
    gui.sundayTimeEdit.setTime(conf[6].time);

    // Exception
    gui  = this.dialog.timeGroupBox.exceptionGroupBox;
    conf = config.exception;

    var js_date = new Date(conf.timestamp);
    var qt_time = new QTime(js_date.getHours(), js_date.getMinutes(), 0, 0);
    gui.setChecked(conf.enabled);
    gui.wakemeupTimeEdit.setTime(qt_time);

    // Music
    gui  = this.dialog.musicGroupBox;
    conf = config.music;

    gui.trackCheckBox.setChecked(conf.track.enabled);
    gui.trackComboBox.setCurrentIndex(conf.track.index);
    gui.progressionCheckBox.setChecked(conf.progression.enabled);
    gui.progressionComboBox.setCurrentIndex(conf.progression.index);

    // Sound
    gui  = this.dialog.soundGroupBox;
    conf = config.sound;

    gui.volumeCheckBox.setChecked(conf.volume.enabled);
    gui.volumeSlider.setValue(conf.volume.value);
    gui.fadeinCheckBox.setChecked(conf.fadein.enabled);
    gui.fadeinSpinBox.setValue(conf.fadein.value);

    // RTC
    gui  = this.dialog.rtcGroupBox;
    conf = config.rtc;

    gui.setChecked(conf.enabled);
    gui.scriptpathLineEdit.setText(conf.scriptpath);

    Amarok.debug("<- Gui." + "setValues");
}

Gui.prototype.getValues = function(config)
{
    Amarok.debug("-> Gui." + "getValues");

    var conf, gui;

    // Usually
    conf = config.usual;
    gui  = this.dialog.timeGroupBox.usualGroupBox;

    conf[0].enabled = gui.mondayCheckBox.checked;
    conf[0].time    = gui.mondayTimeEdit.time;
    conf[1].enabled = gui.tuesdayCheckBox.checked;
    conf[1].time    = gui.tuesdayTimeEdit.time;
    conf[2].enabled = gui.wednesdayCheckBox.checked;
    conf[2].time    = gui.wednesdayTimeEdit.time;
    conf[3].enabled = gui.thursdayCheckBox.checked;
    conf[3].time    = gui.thursdayTimeEdit.time;
    conf[4].enabled = gui.fridayCheckBox.checked;
    conf[4].time    = gui.fridayTimeEdit.time;
    conf[5].enabled = gui.saturdayCheckBox.checked;
    conf[5].time    = gui.saturdayTimeEdit.time;
    conf[6].enabled = gui.sundayCheckBox.checked;
    conf[6].time    = gui.sundayTimeEdit.time;

    // Exception
    conf = config.exception;
    gui  = this.dialog.timeGroupBox.exceptionGroupBox;

    var qt_time = gui.wakemeupTimeEdit.time;
    var js_date_now = new Date();
    var js_date_exception = new Date();
    js_date_exception.setHours(qt_time.hour());
    js_date_exception.setMinutes(qt_time.minute());
    js_date_exception.setSeconds(0);
    js_date_exception.setMilliseconds(0);
    if (js_date_exception < js_date_now) {
	js_date_exception.setDate(js_date_exception.getDate() + 1);
    }
    conf.enabled   = gui.checked;
    conf.timestamp = js_date_exception.getTime();

    // Music
    conf = config.music;
    gui  = this.dialog.musicGroupBox;

    conf.track.enabled       = gui.trackCheckBox.checked;
    conf.track.index         = gui.trackComboBox.currentIndex;
    conf.progression.enabled = gui.progressionCheckBox.checked;
    conf.progression.index   = gui.progressionComboBox.currentIndex;

    // Sound
    conf = config.sound;
    gui  = this.dialog.soundGroupBox;

    conf.volume.enabled = gui.volumeCheckBox.checked;
    conf.volume.value   = gui.volumeSlider.value;
    conf.fadein.enabled = gui.fadeinCheckBox.checked;
    conf.fadein.value   = gui.fadeinSpinBox.value;

    // RTC
    conf = config.rtc;
    gui  = this.dialog.rtcGroupBox;

    conf.enabled    = gui.checked;
    conf.scriptpath = gui.scriptpathLineEdit.text;

    Amarok.debug("<- Gui." + "getValues");
}

Gui.prototype.browse = function()
{
    Amarok.debug("-> Gui." + "browse");

    var line_edit = this.dialog.rtcGroupBox.scriptpathLineEdit;
    var path = QFileDialog.getOpenFileName(this,
					   "Choose a rtcwake script",
					   Amarok.Info.scriptPath() + "/rtc-scripts");
    if (path != "") {
	line_edit.setText(path);
    }

    Amarok.debug("<- Gui." + "browse");
}

Gui.prototype.setStatus = function(message)
{
    Amarok.debug("-> Gui." + "setStatus");

    var label = this.dialog.actionGroupBox.statusLabel;

    label.setText(message);
    label.setStyleSheet("QLabel { color : darkRed; }");

    Amarok.debug("<- Gui." + "setStatus");
}

Gui.prototype.show = function(enabled)
{
    Amarok.debug("-> Gui." + "show");

    var label = this.dialog.actionGroupBox.statusLabel;

    if (enabled == true) {
	// Status label is set by setStatus, we don't have
	// to care about it anymore.
    } else {
	label.setText("Disabled");
        label.setStyleSheet("QLabel { color : darkGreen; }");
    }

    this.dialog.show();

    Amarok.debug("<- Gui." + "show");
}
